FROM openjdk:17-jdk-slim

LABEL maintainer="Moussa Diallo mousse.md10@gmail.com"

EXPOSE 8080

ADD target/labmoussa.jar labmoussa.jar

ENTRYPOINT ["java", "-jar", "labmoussa.jar"]
