package com.groupeisi.gitlabcicd.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hellocicd")
public class Hello {
    public String message() {
        return ("Hello cicd");
    }
}
